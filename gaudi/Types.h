#pragma once

#include "MatrixTypes.h"
#include <string>
#include <iostream>

// Types
enum FilterStatus {Uninitialized, Initialized, Predicted, Filtered, Smoothed};
enum CachedBool   {False, True, Unknown_Cache};
enum Direction    {Forward, Backward};
enum Type         {Unknown_Type, HitOnTrack, Outlier, Reference};

// KalmanFitResult enums
enum algoType {Predict, Filter, Smooth, ComputeResidual, WeightedAverage};
enum errorBits {typeBits=0, algBits=2, dirBits=5, globalBits=7};
enum errorType {Initialization, MatrixInversion, AlgError, Other};

struct XYZVector {
  double fX;
  double fY;
  double fZ;
};

struct ChiSquare {
  double m_chi2;
  int m_nDoF;

  ChiSquare(const double chi2, const int nDoF) : m_chi2(chi2), m_nDoF(nDoF) {}
  ChiSquare() {}
};

struct KalmanFitResult {
  TrackSymMatrix m_seedCovariance;
  int            m_nTrackParameters;
  ChiSquare      m_chi2Velo;
  ChiSquare      m_chi2VeloTT;
  ChiSquare      m_chi2VeloTTT;
  ChiSquare      m_chi2MuonT;
  ChiSquare      m_chi2Muon;
  bool           m_chi2CacheValid;
  unsigned short m_errorFlag;
  bool           m_bidirectionalSmoother;

  // set the error flag out of direction, algorithm and error type identifiers 
  void setErrorFlag(unsigned short direction, unsigned short algnum , unsigned short errnum) 
  {
    m_errorFlag = (((unsigned short) 1 ) << globalBits)
                + (((unsigned short)direction) << dirBits)
                + (((unsigned short)algnum) << algBits)
                + (((unsigned short)errnum) << typeBits);  
  }
};

struct State {
  virtual void foo() {};

  unsigned int   m_flags;
  TrackVector    m_stateVector;
  TrackSymMatrix m_covariance;
  double         m_z;
};

struct StateVector {
  TrackVector m_parameters;
  double      m_z;

  StateVector () = default;
};

struct Node {
  virtual void foo() {};

  Type                  m_type;
  State                 m_state;
  StateVector           m_refVector;
  bool                  m_refIsSet;
  int*                  m_measurement; // Measurement* m_measurement;
  double                m_residual;
  double                m_errResidual;
  double                m_errMeasure;
  TrackProjectionMatrix m_projectionMatrix;
  
  Node () = default;
  Node (const Node& node) = default;
};

struct FitNode : public Node {
  TrackMatrix           m_transportMatrix;       ///< transport matrix for propagation from previous node to this one
  TrackMatrix           m_invertTransportMatrix; ///< transport matrix for propagation from this node to the previous one
  TrackVector           m_transportVector;       ///< transport vector for propagation from previous node to this one
  TrackSymMatrix        m_noiseMatrix;           ///< noise in propagation from previous node to this one
  double                m_deltaEnergy;           ///< change in energy in propagation from previous node to this one
  double                m_refResidual;           ///< residual of the reference    
  FilterStatus          m_filterStatus[2] ;      ///< Status of the Node in the fit process
  CachedBool            m_hasInfoUpstream[2] ;   ///< Are the nodes with active measurement upstream of this node?
  State                 m_predictedState[2];     ///< predicted state of forward/backward filter
  State                 m_filteredState[2];      ///< filtered state of forward filter
  State                 m_classicalSmoothedState;
  ChiSquare             m_deltaChi2[2];          ///< chisq contribution in forward filter
  ChiSquare             m_totalChi2[2];          ///< total chi2 after this filterstep 
  TrackMatrix           m_smootherGainMatrix;   ///< smoother gain matrix (smoothedfit only)
  XYZVector             m_pocaVector;           ///< unit vector perpendicular to state and measurement
  double                m_doca;                 ///< signed doca (of ref-traj). for ST/velo this is equal to minus (ref)residual 
  FitNode*              m_prevNode;              ///< Previous Node
  FitNode*              m_nextNode;              ///< Next Node
  KalmanFitResult*      m_parent;               ///< Owner
  double                m_chi2Double;
  int                   m_parent_nTrackParameters;

  // Conditions for predict / filter state execution
  bool requirePredictedState (int direction) const { return m_filterStatus[direction] < Predicted; }
  bool requireFilteredState  (int direction) const { return m_filterStatus[direction] < Filtered; }
  bool requireFilter () const { return m_type == HitOnTrack; }
  
  // Update predicted with filtered
  void updateFilteredWithPredicted(int direction) { m_filteredState[direction] = m_predictedState[direction]; }

  FitNode () = default;
  FitNode (const FitNode& node) = default;
};

void operator+= (ChiSquare& c0, const ChiSquare& c1);
