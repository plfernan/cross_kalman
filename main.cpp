#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cassert>
#include <map>
#include <array>
#include <deque>
#include <unistd.h>

#include "gaudi/Types.h"

#include "utilities/Instance.h"
#include "utilities/Timer.h"
#include "utilities/FileReader.h"
#include "utilities/MainAux.h"
#include "utilities/Print.h"
#include "utilities/Compare.h"

#include "fit/Types.h"
#include "fit/Scheduler.h"
#include "fit/MemView.h"

#include "fit/scalar/Predict.h"
#include "fit/scalar/Update.h"
#include "fit/scalar/Combined.h"
#include "fit/scalar/Smoother.h"

#ifndef NO_VECTOR_BACKEND
#include "fit/vector/Predict.h"
#include "fit/vector/Update.h"
#include "fit/vector/Smoother.h"
#endif

#ifdef CALLGRIND_ON
#include <valgrind/callgrind.h>
#endif
#ifdef TBB_ON
#include "tbb/tbb.h"
#endif

#ifdef USE_VCL
  #pragma message("Generating code with vector width " TO_STRING(VECTOR_WIDTH) ", " TO_STRING(PRECISION) " precision. Vector backend: VCL")
#elif defined(USE_UMESIMD)
  #pragma message("Generating code with vector width " TO_STRING(VECTOR_WIDTH) ", " TO_STRING(PRECISION) " precision. Vector backend: UMESIMD")
#else
  #pragma message("Generating scalar code")
#endif

int main(int argc, char **argv) {
  std::string foldername = "events";
  std::vector<std::vector<Instance>> events;
  unsigned bitEpsilonAllowed = 50;
  unsigned numExperiments = 10000;
  unsigned maxNumFilesToOpen = 0;
  unsigned minNumTracksInInstance = 0;
  unsigned verbose = 0;
  unsigned warmupIterations = 0;
  unsigned minTrackBatchSize = 8;
  bool printFitNodesWhenDifferent = false;
  bool checkResults = false;
  bool allEqual = true;
  bool experimentMode = false;
  bool vectorised_opt = true;
  bool printTiming = false;
  TimePrinter p (true);
  FileReader fr;
  int8_t c;
  double startingTime = Timer::getCurrentTime();

  constexpr unsigned defaultReservedMemory = 1024;

  while (true) {
    c = getopt (argc, argv, "b:f:n:h?m:c:v:p:es:i:t:w:");
    if (c == -1) break; // Note: Test it works with P8

    switch (c) {
      case 'n':
        numExperiments = convertAmount(optarg);
        break;
      case 't':
        printTiming = (bool) atoi(optarg);
        break;
      case 'i':
        minTrackBatchSize = atoi(optarg);
        break;
      case 'f':
        foldername = optarg;
        break;
      case 'b':
        bitEpsilonAllowed = atoi(optarg);
        break;
      case 'm':
        maxNumFilesToOpen = atoi(optarg);
        break;
      case 'c':
        checkResults = (bool) atoi(optarg);
        break;
      case 'v':
        verbose = atoi(optarg);
        break;
      case 's':
        vectorised_opt = not ((bool) atoi(optarg));
        break;
      case 'w':
        warmupIterations = convertAmount(optarg);
        break;
      case 'p':
      {
        const unsigned int percentile = atoi(optarg);
        p = TimePrinter(true, percentile, 100 - percentile);
        break;
      }
      case 'e':
        experimentMode = true;
        break;
      case 'h':
      case '?':
        std::cout << "kalfit [-f foldername=events] [-w warmupIterations=0] [-n numExperiments=10000]"
          << " [-s sequential=0 (vectorised)] [-i minTrackBatchSize=8]"
          << " [-m maxNumFilesToOpen=0 (all)] [-e (print git checkout version=false)]"
          << " [-p topBottomPercentile=3] [-t printTiming=0]"
          << " [-c checkResults=0 (0,1) [-b bitEpsilonAllowed=50] [-v verbose=0 (0,1,2)]]" << std::endl;
        return 0;
      default:
        break;
    }
  }

#if VECTOR_WIDTH == 1u
  vectorised_opt = false;
#endif

  if (experimentMode) {
    std::cout << "Reported version (git): " << shellexec("git rev-parse HEAD");
  }

  std::cout << "Running options: ";
  for (int i=0; i<argc; ++i) std::cout << argv[i] << " ";
  std::cout << std::endl << std::endl;

  // Read events
  VectorFit::Mem::Store memManager (41, defaultReservedMemory);
  {
    // files will be deleted - we don't want to waste a lot of memory on this
    std::vector<std::vector<uint8_t>> files;
    fr.readFilesFromFolder(files, foldername, maxNumFilesToOpen);
    for (const auto& f : files) {
      events.push_back(translateFileIntoEvent(f, memManager, checkResults, minTrackBatchSize));
    }
  }
  assert(events.size() > 0);
  
  // Reserve memory pointers
  VectorFit::Mem::Store nodeParametersStore {VectorFit::Mem::View::NodeParameters::size(), defaultReservedMemory};
  VectorFit::Mem::Store transportForwardStore {VectorFit::Mem::View::TrackMatrix<25>::size(), defaultReservedMemory};
  VectorFit::Mem::Store transportBackwardStore {VectorFit::Mem::View::TrackMatrix<25>::size(), defaultReservedMemory};

  // Iterators to know where we are reading
  std::vector<std::vector<VectorFit::Mem::Iterator>> nodeParametersIteratorInit (events.size());
  std::vector<std::vector<VectorFit::Mem::Iterator>> transportForwardIteratorInit (events.size());
  std::vector<std::vector<VectorFit::Mem::ReverseIterator>> nodeParametersReverseIteratorInit (events.size());
  std::vector<std::vector<VectorFit::Mem::ReverseIterator>> transportBackwardIteratorInit (events.size());
  std::vector<std::vector<std::array<std::vector<VectorFit::Sch::Blueprint<VECTOR_WIDTH>>, 5>>> schedulers (events.size());

  // Initialize scheduler and iterators
  initializeSchedulerAndIterators(
    events,
    schedulers,
    nodeParametersIteratorInit,
    transportForwardIteratorInit,
    nodeParametersReverseIteratorInit,
    transportBackwardIteratorInit,
    nodeParametersStore,
    transportForwardStore,
    transportBackwardStore
  );

  // Clear typecasted tracks (not needed anymore)
  clearTypecastedTracks(events);

  // Print some info for the events
  std::cout << "Read " << events.size() << " events" << std::endl;
  for (unsigned i=0; i<events.size(); ++i) {
    const auto& event = events[i];

    std::cout << "Event #" << i << ":" << std::endl;
    printInfo(event);
  }

  // We should run "as the framework should"
  // So we run many events, scheduled with tbb,
  // exploiting parallelism in the best way possible
  // within each event, within a single core
  
  // Prepare the memory manager
  unsigned totalNodes = 0;
  size_t maxNodeLength = 0;

  for (auto& event : events) {
    for (auto& instance : event) {
      totalNodes = std::max(totalNodes, [&] { unsigned acc=0; for (const auto& t : instance.tracks) acc += t.m_nodes.size(); return acc; } ());
      maxNodeLength = std::max(maxNodeLength, [&] { size_t m=0; for (const auto& t : instance.tracks) m = std::max(m, t.m_nodes.size()); return m; } ()) / 2.0;
    }
  }
  const unsigned reservedMem = totalNodes + maxNodeLength * VECTOR_WIDTH;
  const unsigned reservedMemSeq = vectorised_opt ? reservedMem : reservedMem * 2;
  const unsigned reservedMemVec = vectorised_opt ? reservedMem : 1;

#ifdef TBB_ON
  const int numberOfThreads = tbb::task_scheduler_init::default_num_threads();
  tbb::enumerable_thread_specific<VectorFit::Mem::Store> threadmemseq (VectorFit::Mem::Store(VectorFit::Mem::View::State::size(), reservedMemSeq));
  tbb::enumerable_thread_specific<VectorFit::Mem::Store> threadmemvecforward (VectorFit::Mem::Store(VectorFit::Mem::View::State::size(), reservedMem));
  tbb::enumerable_thread_specific<VectorFit::Mem::Store> threadmemvecbackward (VectorFit::Mem::Store(VectorFit::Mem::View::State::size(), reservedMem));
  tbb::enumerable_thread_specific<VectorFit::Mem::Store> threadmemvecpost (VectorFit::Mem::Store(VectorFit::Mem::View::State::size(), reservedMemVec));
  tbb::enumerable_thread_specific<VectorFit::Mem::Store> threadmemvecsmoother (VectorFit::Mem::Store(VectorFit::Mem::View::SmoothState::size(), reservedMem));
  tbb::enumerable_thread_specific<std::pair<double, double>> threadStartEnd (std::make_pair(0.0, 0.0));
  std::vector<tbb::concurrent_vector<std::pair<int, Timer>>> timers (2);
  tbb::mutex mutex;
#else
  const int numberOfThreads = 1;
  std::vector<std::vector<std::pair<int, Timer>>> timers (2);
  VectorFit::Mem::Store memseq (VectorFit::Mem::View::State::size(), reservedMemSeq);
  VectorFit::Mem::Store memvecforward (VectorFit::Mem::View::State::size(), reservedMem);
  VectorFit::Mem::Store memvecbackward (VectorFit::Mem::View::State::size(), reservedMem);
  VectorFit::Mem::Store memvecpost (VectorFit::Mem::View::State::size(), reservedMemVec);
  VectorFit::Mem::Store memvecsmoother (VectorFit::Mem::View::SmoothState::size(), reservedMem);
#endif

  std::cout << "Running experiments ..." << std::endl;

#ifdef TBB_ON
  tbb::parallel_for (static_cast<unsigned int>(0), static_cast<unsigned int>(numExperiments),
  [&] (unsigned int i) {

    VectorFit::Mem::Store& memseq = threadmemseq.local();
    VectorFit::Mem::Store& memvecforward = threadmemvecforward.local();
    VectorFit::Mem::Store& memvecbackward = threadmemvecbackward.local();
    VectorFit::Mem::Store& memvecsmoother = threadmemvecsmoother.local();
    VectorFit::Mem::Store& memvecpost = threadmemvecpost.local();
    if (threadStartEnd.local().first == 0.0) {
      threadStartEnd.local().first = Timer::getCurrentTime();
    }
#else
  for (unsigned i=0; i<numExperiments; ++i) {
#endif

    // Note: Copying the event requires too much memory, rather copy only what we need
    //       We copy so as not to pollute the events
    const unsigned eventno = i % events.size(); // Round robin event selection
    const auto& event = events[eventno];
    for (unsigned instanceno=0; instanceno<event.size(); ++instanceno) {
      const auto& instance = event[instanceno];

      // Prepare experiment
      // Note: Copy the tracks, which is exactly what we need for our experiment
      auto tracks = instance.tracks;
      std::vector<uint16_t> schedulingMasks {0xFFFF};

      // Default to sequential execution if number of tracks is less than vector width
      const bool vectorised = (tracks.size() >= VECTOR_WIDTH) ? vectorised_opt : false;

      memseq.reset();
      memvecforward.reset();
      memvecbackward.reset();
      memvecsmoother.reset();
      memvecpost.reset();

      // Overhead timer
      auto tOverhead = Timer();
      tOverhead.start();
      tOverhead.stop();
      timers[1].push_back(std::make_pair(1, tOverhead));

      // Copying the iterators for experiment
      VectorFit::Mem::Iterator nodeParametersIterator (nodeParametersIteratorInit[eventno][instanceno]);
      VectorFit::Mem::Iterator transportForwardIterator (transportForwardIteratorInit[eventno][instanceno]);
      VectorFit::Mem::ReverseIterator nodeParametersReverseIterator (nodeParametersReverseIteratorInit[eventno][instanceno]);
      VectorFit::Mem::ReverseIterator transportBackwardIterator (transportBackwardIteratorInit[eventno][instanceno]);

      auto tFit = Timer();
      tFit.start();

#ifdef CALLGRIND_ON
    CALLGRIND_TOGGLE_COLLECT;
#endif

#ifndef NO_VECTOR_BACKEND
      if (vectorised) {
        // Schedule what we are going to do
        auto& scheduler_main = schedulers[eventno][instanceno][VectorFit::Sch::Num::Main];
        auto& scheduler_forward_init = schedulers[eventno][instanceno][VectorFit::Sch::Num::ForwardInit];
        auto& scheduler_backward_init = schedulers[eventno][instanceno][VectorFit::Sch::Num::BackwardInit];

        if (verbose>2) {
          std::cout << scheduler_forward_init << std::endl
            << scheduler_main << std::endl
            << scheduler_backward_init << std::endl;
        }
        
        // Forward fit
        //  Forward initialization
        std::for_each (scheduler_forward_init.begin(), scheduler_forward_init.end(), [&] (decltype(scheduler_forward_init[0])& s) {
          const auto& action = s.action;
          const auto& pool = s.pool;

          // Set memory location, do predict (always in a new vector, no need to swap)
          const auto& vector = memvecforward.getNewVector();
          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (action[i]) {
              auto& track = tracks[pool[i].trackIndex];
              auto& node = track.m_nodes[pool[i].nodeIndex];

              // Update pointer
              node.m_forwardState.setBasePointer(vector + i);
            }
          }

          // Fetch initial state
          _aligned std::array<PRECISION, 5*VECTOR_WIDTH> initialState = 
            VectorFit::ArrayGen<VectorFit::Op::Forward>::generateInitialState<VECTOR_WIDTH>(pool, tracks);

          const auto npVector = VectorFit::Mem::View::NodeParameters {nodeParametersIterator.nextVector()};
          VectorFit::Mem::View::State current (vector);

          // Update
          VectorFit::Vector::update<VECTOR_WIDTH> (
            pool,
            tracks,
            initialState.data(),
            VectorFit::m_initialVectorCovariance.data(),
            npVector.m_referenceVector.m_basePointer,
            npVector.m_projectionMatrix.m_basePointer,
            npVector.m_referenceResidual,
            npVector.m_errorMeasure,
            current.m_updatedState.m_basePointer,
            current.m_updatedCovariance.m_basePointer,
            current.m_chi2
          );
        });

        // Forward fit
        //  Main iterations
        std::vector<VectorFit::SwapStore> swaps;
        memvecforward.getNewVector();
        std::for_each (scheduler_main.begin(), scheduler_main.end(), [&] (decltype(scheduler_main[0])& s) {
          const auto& in  = s.in;
          const auto& out = s.out;
          const auto& action = s.action;
          const auto& pool = s.pool;

          // Feed the data we need in our vector
          const auto& lastVector = memvecforward.getLastVector();
          if (in.any()) {
            for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
              if (in[i]) {
                auto& track = tracks[pool[i].trackIndex];
                auto& prevnode = track.m_nodes[pool[i].nodeIndex - 1];

                VectorFit::Mem::View::State memslot (lastVector + i);
                memslot.m_updatedState.copy(prevnode.get<VectorFit::Op::Forward, VectorFit::Op::StateVector>());
                memslot.m_updatedCovariance.copy(prevnode.get<VectorFit::Op::Forward, VectorFit::Op::Covariance>());
              }
            }
          }

          // update the pointers requested
          const auto& vector = memvecforward.getNewVector();
          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (action[i]) {
              auto& track = tracks[pool[i].trackIndex];
              auto& node = track.m_nodes[pool[i].nodeIndex];
              
              node.m_forwardState.setBasePointer(vector + i);
            }
          }

          const auto npVector = VectorFit::Mem::View::NodeParameters{nodeParametersIterator.nextVector()};

          // predict and update
          VectorFit::Mem::View::State last (lastVector);
          VectorFit::Mem::View::State current (vector);

          VectorFit::Vector::predict<VectorFit::Op::Forward>::op<VECTOR_WIDTH> (
            transportForwardIterator.nextVector(),
            last.m_updatedState.m_basePointer,
            last.m_updatedCovariance.m_basePointer,
            npVector.m_noiseMatrix.m_basePointer,
            npVector.m_transportVector.m_basePointer,
            current.m_updatedState.m_basePointer,
            current.m_updatedCovariance.m_basePointer
          );

          VectorFit::Vector::update<VECTOR_WIDTH> (
            pool,
            tracks,
            npVector.m_referenceVector.m_basePointer,
            npVector.m_projectionMatrix.m_basePointer,
            npVector.m_referenceResidual,
            npVector.m_errorMeasure,
            current.m_updatedState.m_basePointer,
            current.m_updatedCovariance.m_basePointer,
            current.m_chi2
          );

          // Move requested data out
          if (out.any()) {
            for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
              if (out[i]) {
                auto& track = tracks[pool[i].trackIndex];
                auto& node = track.m_nodes[pool[i].nodeIndex];

                VectorFit::Mem::View::State memslot (memseq.getNextElement());
                memslot.m_updatedState.copy(node.get<VectorFit::Op::Forward, VectorFit::Op::StateVector>());
                memslot.m_updatedCovariance.copy(node.get<VectorFit::Op::Forward, VectorFit::Op::Covariance>());
                swaps.push_back(
                  VectorFit::SwapStore(
                    memslot,
                    node.get<VectorFit::Op::Forward, VectorFit::Op::StateVector>(),
                    node.get<VectorFit::Op::Forward, VectorFit::Op::Covariance>()
                  )
                );
              }
            }
          }
        });

        // Restore updated state
        for (auto& swap : swaps) {
          swap.state.copy(swap.store.m_updatedState);
          swap.covariance.copy(swap.store.m_updatedCovariance);
        }

        // Forward fit
        //  Last iterations
        std::for_each(tracks.begin(), tracks.end(), [&] (VectorFit::Track& track) {
          auto& nodes = track.m_nodes;
          for (unsigned i=track.m_nodes.size() - track.m_backwardUpstream - 1; i<track.m_nodes.size(); ++i) {
            VectorFit::FitNode& prevnode = nodes[i-1];
            VectorFit::FitNode& node = nodes[i];
            node.m_forwardState.setBasePointer(memseq.getNextElement());
            VectorFit::Scalar::fit<VectorFit::Op::Forward, true>(node, prevnode);
          }
        });

        // Backward fit and smoother
        //  Backward initialization
        std::for_each (scheduler_backward_init.rbegin(), scheduler_backward_init.rend(), [&] (decltype(scheduler_backward_init[0])& s) {
          const auto& action = s.action;
          const auto& pool = s.pool;

          // Set memory location, do predict (always in a new vector, no need to swap)
          const auto& vector = memseq.getNewVector();
          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (action[i]) {
              auto& track = tracks[pool[i].trackIndex];
              auto& node = track.m_nodes[pool[i].nodeIndex];

              // Update pointer
              node.m_backwardState.setBasePointer(vector + i);
            }
          }

          // Fetch initial state
          _aligned std::array<PRECISION, 5*VECTOR_WIDTH> initialState = 
            VectorFit::ArrayGen<VectorFit::Op::Backward>::generateInitialState<VECTOR_WIDTH>(pool, tracks);

          const auto npVector = VectorFit::Mem::View::NodeParameters{nodeParametersReverseIterator.previousVector()};
          VectorFit::Mem::View::State current (vector);

          // Update
          VectorFit::Vector::update<VECTOR_WIDTH> (
            pool,
            tracks,
            initialState.data(),
            VectorFit::m_initialVectorCovariance.data(),
            npVector.m_referenceVector.m_basePointer,
            npVector.m_projectionMatrix.m_basePointer,
            npVector.m_referenceResidual,
            npVector.m_errorMeasure,
            current.m_updatedState.m_basePointer,
            current.m_updatedCovariance.m_basePointer,
            current.m_chi2
          );
        });

        // Backward fit and smoother
        //  Main iterations
        memvecbackward.getNewVector();
        VectorFit::Mem::ReverseIterator forwardReverseIterator (memvecforward);
        std::for_each (scheduler_main.rbegin(), scheduler_main.rend(), [&] (decltype(scheduler_main[0])& s) {
          const auto& in  = s.out;
          const auto& out = s.in;
          const auto& action = s.action;
          const auto& pool = s.pool;

          // Feed the data we need in our vector
          const auto& lastVector = memvecbackward.getLastVector();
          if (in.any()) {
            for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
              if (in[i]) {
                auto& track = tracks[pool[i].trackIndex];
                auto& prevnode = track.m_nodes[pool[i].nodeIndex+1];

                VectorFit::Mem::View::State memslot (lastVector + i);
                memslot.m_updatedState.copy(prevnode.get<VectorFit::Op::Backward, VectorFit::Op::StateVector>());
                memslot.m_updatedCovariance.copy(prevnode.get<VectorFit::Op::Backward, VectorFit::Op::Covariance>());
              }
            }
          }

          // update the pointers requested
          const auto& vector = memvecbackward.getNewVector();
          const auto& smootherVector = memvecsmoother.getNewVector();
          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (action[i]) {
              auto& track = tracks[pool[i].trackIndex];
              auto& node = track.m_nodes[pool[i].nodeIndex];

              node.m_backwardState.setBasePointer(vector + i);
              node.m_smoothState.setBasePointer(smootherVector + i);
            }
          }

          const auto npVector = VectorFit::Mem::View::NodeParameters{nodeParametersReverseIterator.previousVector()};

          // predict and update
          VectorFit::Mem::View::State last (lastVector);
          VectorFit::Mem::View::State current (vector);

          VectorFit::Vector::predict<VectorFit::Op::Backward>::op<VECTOR_WIDTH> (
            pool,
            tracks,
            transportBackwardIterator.previousVector(),
            last.m_updatedState.m_basePointer,
            last.m_updatedCovariance.m_basePointer,
            current.m_updatedState.m_basePointer,
            current.m_updatedCovariance.m_basePointer
          );

          // Do the smoother *here*
          // This benefits from cache locality,
          // and removes the need of doing swapping to restore
          // the updated states
          
          // Fetch a new memvecsmoother vector
          VectorFit::Mem::View::SmoothState smooth (smootherVector);
          VectorFit::Mem::View::State forward (forwardReverseIterator.previousVector());

          // Smoother
          uint16_t smoother_result = VectorFit::Vector::smoother<VECTOR_WIDTH> (
            forward.m_updatedState.m_basePointer,
            forward.m_updatedCovariance.m_basePointer,
            current.m_updatedState.m_basePointer,      // These are still the predicted state
            current.m_updatedCovariance.m_basePointer, // and covariance
            smooth.m_state.m_basePointer,
            smooth.m_covariance.m_basePointer
          );

          // Update residuals
          VectorFit::Vector::updateResiduals (
            pool,
            tracks,
            npVector.m_referenceVector.m_basePointer,
            npVector.m_projectionMatrix.m_basePointer,
            npVector.m_referenceResidual,
            npVector.m_errorMeasure,
            smooth.m_state.m_basePointer,
            smooth.m_covariance.m_basePointer,
            smooth.m_residual,
            smooth.m_errResidual
          );

          // Update
          VectorFit::Vector::update<VECTOR_WIDTH> (
            pool,
            tracks,
            npVector.m_referenceVector.m_basePointer,
            npVector.m_projectionMatrix.m_basePointer,
            npVector.m_referenceResidual,
            npVector.m_errorMeasure,
            current.m_updatedState.m_basePointer,
            current.m_updatedCovariance.m_basePointer,
            current.m_chi2
          );

#ifdef DEBUG
          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (not action[i]) {
              smoother_result |= 1 << i;
            }
          }
          if (smoother_result != VectorFit::ArrayGenCommon::mask<VECTOR_WIDTH>()) {
            std::cout << "smoother errors: ";
            print(smoother_result);
            std::cout << std::endl;
          }
#endif
          // Move requested data out and update data pointers
          if (out.any()) {
           for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
             if (out[i]) {
               auto& track = tracks[pool[i].trackIndex];
               auto& node = track.m_nodes[pool[i].nodeIndex];

               VectorFit::Mem::View::State memslot (memseq.getNextElement());
                memslot.m_updatedState.copy(node.get<VectorFit::Op::Backward, VectorFit::Op::StateVector>());
                memslot.m_updatedCovariance.copy(node.get<VectorFit::Op::Backward, VectorFit::Op::Covariance>());
               node.get<VectorFit::Op::Backward, VectorFit::Op::StateVector>().setBasePointer(memslot.m_updatedState);
               node.get<VectorFit::Op::Backward, VectorFit::Op::Covariance>().setBasePointer(memslot.m_updatedCovariance);
             }
           }
          }
        });

        // Backward fit
        //  Last iterations
        std::for_each(tracks.begin(), tracks.end(), [&] (VectorFit::Track& track) {
          auto& nodes = track.m_nodes;
          for (unsigned i=0; i<=track.m_forwardUpstream; ++i) {
            const int element = track.m_forwardUpstream - i;
            VectorFit::FitNode& prevnode = nodes[element + 1];
            VectorFit::FitNode& node = nodes[element];
            node.m_backwardState.setBasePointer(memseq.getNextElement());
            VectorFit::Scalar::fit<VectorFit::Op::Backward, true>(node, prevnode);
          }
        });
      }
      else {
#endif
        // Forward fit
        std::for_each(tracks.begin(), tracks.end(), [&] (VectorFit::Track& track) {

          // this is just the first iteration, because we need to copy the covariance
          auto& nodes = track.m_nodes;
          VectorFit::FitNode& node = nodes.front();
          node.m_forwardState.setBasePointer(memseq.getNextElement());
          VectorFit::Scalar::fit<VectorFit::Op::Forward>(node, track.m_initialForwardCovariance);

          // Before m_forwardUpstream
          // this is the pre
          for (unsigned i=1; i<=track.m_forwardUpstream; ++i) {
            VectorFit::FitNode& prevnode = nodes[i-1];
            VectorFit::FitNode& node = nodes[i];
            node.m_forwardState.setBasePointer(memseq.getNextElement());
            VectorFit::Scalar::fit<VectorFit::Op::Forward, false>(node, prevnode);
          }

          // After m_forwardUpstream
          // this is the main && post
          for (unsigned i=track.m_forwardUpstream+1; i<nodes.size(); ++i) {
            VectorFit::FitNode& prevnode = nodes[i-1];
            VectorFit::FitNode& node = nodes[i];
            node.m_forwardState.setBasePointer(memseq.getNextElement());
            VectorFit::Scalar::fit<VectorFit::Op::Forward, true>(node, prevnode);
          }
        });

        // Backward fit
        std::for_each(tracks.begin(), tracks.end(), [&] (VectorFit::Track& track) {
          auto& nodes = track.m_nodes;
          VectorFit::FitNode& node = nodes.back();
          node.m_backwardState.setBasePointer(memseq.getNextElement());
          VectorFit::Scalar::fit<VectorFit::Op::Backward>(node, track.m_initialBackwardCovariance);

          // Before m_backwardUpstream
          for (unsigned i=1; i<=track.m_backwardUpstream; ++i) {
            const int element = track.m_nodes.size() - i - 1;
            VectorFit::FitNode& prevnode = nodes[element + 1];
            VectorFit::FitNode& node = nodes[element];
            node.m_backwardState.setBasePointer(memseq.getNextElement());
            VectorFit::Scalar::fit<VectorFit::Op::Backward, false>(node, prevnode);
          }

          // After m_backwardUpstream
          for (unsigned i=track.m_backwardUpstream+1; i<nodes.size(); ++i) {
            const int element = track.m_nodes.size() - i - 1;
            VectorFit::FitNode& prevnode = nodes[element + 1];
            VectorFit::FitNode& node = nodes[element];
            node.m_backwardState.setBasePointer(memseq.getNextElement());

            // we make the smoother in between to benefit from having the 
            // predict and update here
            VectorFit::Scalar::predict<VectorFit::Op::Backward, true>(node, prevnode);

            // // Do the smoother
            if (i < (track.m_nodes.size() - track.m_forwardUpstream - 1)) {
              node.m_smoothState.setBasePointer(memvecsmoother.getNextElement());
              VectorFit::Scalar::smoother<true, true>(node);
              VectorFit::Scalar::updateResiduals(node);
            }

            VectorFit::Scalar::update<VectorFit::Op::Backward>(node);
          }
        });
#ifndef NO_VECTOR_BACKEND
      }
#endif

      // Calculate the chi2 a posteriori
      std::for_each(tracks.begin(), tracks.end(), [&] (VectorFit::Track& track) {
        auto& nodes = track.m_nodes;
        track.m_ndof = -track.m_parent_nTrackParameters;
        for (unsigned k=0; k<nodes.size(); ++k) {
          const VectorFit::FitNode& node = nodes[k];

          if (node.m_type == HitOnTrack) {
            ++track.m_ndof;
            track.m_forwardFitChi2  += node.get<VectorFit::Op::Forward, VectorFit::Op::Chi2>();
            track.m_backwardFitChi2 += node.get<VectorFit::Op::Backward, VectorFit::Op::Chi2>();
          }
        }
      });

      // Smoother - not upstream iterations
      std::for_each(tracks.begin(), tracks.end(), [&] (VectorFit::Track& track) {
        for (unsigned j=0; j<=track.m_forwardUpstream; ++j) {
          track.m_nodes[j].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
          VectorFit::Scalar::smoother<false, true>(track.m_nodes[j]);
          VectorFit::Scalar::updateResiduals(track.m_nodes[j]);
        }

        for (unsigned j=0; j<=track.m_backwardUpstream; ++j) {
          const unsigned element = track.m_nodes.size() - j - 1;
          track.m_nodes[element].m_smoothState.setBasePointer(memvecsmoother.getNextElement());
          VectorFit::Scalar::smoother<true, false>(track.m_nodes[element]);
          VectorFit::Scalar::updateResiduals(track.m_nodes[element]);
        }
      });

      tFit.stop();

#ifdef CALLGRIND_ON
      CALLGRIND_TOGGLE_COLLECT;
#endif

      // Add fit timer to list
      // Number of processes refers to our metric
      // Here, we define a process as a single (forward, backward, smooth) on a node
      // So effectively, number of processes equals number of nodes
      unsigned numberOfProcesses = 0;
      std::for_each(tracks.begin(), tracks.end(), [&] (VectorFit::Track& track) {
        numberOfProcesses += track.m_nodes.size();
      });
      timers[0].push_back(std::make_pair(numberOfProcesses, tFit));

#ifdef TBB_ON
      // Fill in end timer
      threadStartEnd.local().second = Timer::getCurrentTime();
#endif
    
      // Convert back
      if (checkResults) {
        auto bitEpsilon = pow(2.0, ((double) bitEpsilonAllowed));

        for (const auto& track : tracks) {
          // With the last modification, tracks with m_backwardUpstream == 0
          // will most likely fail in the checks. This is acceptable (Wouter),
          // so just don't check those.
          if (track.m_backwardUpstream != 0) {
            const std::vector<FitNode>& result = instance.expectedResult[track.m_index];

            std::vector<FitNode> convertedTrack;
            for (const auto& node : track.m_nodes) {
              convertedTrack.push_back(node.operator FitNodeAOS());
            }

            convertedTrack.back().m_totalChi2[Forward] = ChiSquare(track.m_forwardFitChi2, track.m_ndof);
            convertedTrack.front().m_totalChi2[Backward] = ChiSquare(track.m_backwardFitChi2, track.m_ndof);
#ifdef TBB_ON
            tbb::mutex::scoped_lock lock (mutex);
#endif
            for (unsigned nodeNumber=0; nodeNumber<convertedTrack.size(); ++nodeNumber) {
              int compareChi2 = -1;
              if (nodeNumber==0) compareChi2 = Backward;
              else if (nodeNumber==convertedTrack.size()-1) compareChi2 = Forward;

              const bool comparison = compare(
                convertedTrack[nodeNumber],
                result[nodeNumber],
                bitEpsilon,
                (verbose > 0),
                compareChi2
              );

              if (!comparison) {
                std::cout << "Mismatch in event #" << eventno << ", instance #" << instanceno
                  << ", track " << track.m_index << ", node " << nodeNumber << std::endl;
                allEqual = false;

                if (verbose > 1) {
                  std::cout << std::endl;
                  print(convertedTrack[nodeNumber]);
                  std::cout << std::endl;
                  print(result[nodeNumber]);
                  std::cout << std::endl;
                }
              }
            }
          }
        }
      }
    }
#ifdef TBB_ON
  });
#else
  }
#endif
  
#ifdef CALLGRIND_ON
  CALLGRIND_DUMP_STATS;
#endif

  std::cout << std::endl;

#ifdef TBB_ON
  std::pair<double, double> concurrentTime (std::make_pair(0.0, threadStartEnd.begin()->second));
  for (auto it=threadStartEnd.begin(); it!=threadStartEnd.end(); ++it) {
    concurrentTime.first = std::max(concurrentTime.first, it->first);
    concurrentTime.second = std::min(concurrentTime.second, it->second);
  }

  if (concurrentTime.first > concurrentTime.second) {
    std::cout << "Warning: There was no concurrent time window." << std::endl
    << "Please, increase the number of experiments by setting -n <numExperiments>." << std::endl << std::endl;
  }
  else {
    std::cout << "Concurrent time window since start of experiments (s): "
      << (concurrentTime.first - startingTime) << ", " << (concurrentTime.second - startingTime)
      << std::endl;

    decltype(timers) pruned_timers (2);

    // Just keep the timers inside this window
    for (auto it=timers[0].begin(); it!=timers[0].end(); ++it) {
      const auto& timer = it->second;
      // std::cout << "Start time: " << (timer.getStartTime() - startingTime) << std::endl;
      // std::cout << "End time: " << (timer.getEndTime() - startingTime) << std::endl;
      if (timer.getStartTime() > concurrentTime.first and timer.getEndTime() < concurrentTime.second) {
        pruned_timers[0].push_back(*it);
      }
    }

    for (auto it=timers[1].begin(); it!=timers[1].end(); ++it) {
      const auto& timer = it->second;
      if (timer.getStartTime() > concurrentTime.first and timer.getEndTime() < concurrentTime.second) {
        pruned_timers[1].push_back(*it);
      }
    }

    std::cout << "Number of original and pruned timers: " << timers[0].size() << ", " << pruned_timers[0].size() << std::endl << std::endl;
    timers = pruned_timers;
  }
#endif

  std::map<std::string, double> fitsTimes     = p.printWeightedTimer(timers[0], "Fit", printTiming);
  std::map<std::string, double> overheadTimes = p.printWeightedTimer(timers[1], "Overhead", printTiming);
  std::cout << std::endl;
  const double fullTime = fitsTimes["rawsum"] - overheadTimes["rawsum"];
  const unsigned long long processedNodes = [&] {unsigned long long t=0; for (const auto& wt : timers[0]) t += wt.first; return t;} ();

  if (checkResults) {
    if (allEqual) {
      std::cout << "Check results succeeded!" << std::endl
        << "All of the experiments gave the expected results" << std::endl;
    }
    else {
      std::cout << "Check results failed" << std::endl
        << "Some experiments did not yield the expected results" << std::endl
        << "Try running with -c 1 -v 1 to see what happens" << std::endl;
    }
  }

  std::cout << "Total statistics: " << processedNodes << " nodes processed in " << fullTime << " s" << std::endl;

  std::cout << std::endl
    << "tbb default_num_threads reports " << numberOfThreads << " available threads" << std::endl
    << "Throughput per processor, estimated total throughput:" << std::endl;

  // Metric is #(forward, backward, smooth) / time:
  const double fullThroughput = ((double) processedNodes) / fullTime;
  std::cout << " Fit throughput: " << fullThroughput << " / s, "
    << numberOfThreads * fullThroughput << " / s" << std::endl;

  return 0;
}
