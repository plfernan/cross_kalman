#pragma once

using namespace Mem::View;

// ---------------
// get methods
// ---------------

template<>
inline const PRECISION& FitNode::get<Op::NodeParameters, Op::ReferenceResidual> () const {
  return *(m_nodeParameters.m_referenceResidual);
}

template<>
inline const PRECISION& FitNode::get<Op::NodeParameters, Op::ErrMeasure> () const {
  return *(m_nodeParameters.m_errorMeasure);
}

template<>
inline const TrackVector& FitNode::get<Op::NodeParameters, Op::ReferenceVector> () const {
  return m_nodeParameters.m_referenceVector;
}

template<>
inline const TrackVector& FitNode::get<Op::NodeParameters, Op::ProjectionMatrix> () const {
  return m_nodeParameters.m_projectionMatrix;
}

template<>
inline const TrackSymMatrix& FitNode::get<Op::NodeParameters, Op::NoiseMatrix> () const {
  return m_nodeParameters.m_noiseMatrix;
}

template<>
inline const TrackVector& FitNode::get<Op::NodeParameters, Op::TransportVector> () const {
  return m_nodeParameters.m_transportVector;
}

template<>
inline const PRECISION& FitNode::get<Op::Forward, Op::Chi2> () const {
  return *m_forwardState.m_chi2;
}

template<>
inline const TrackVector& FitNode::get<Op::Forward, Op::StateVector> () const {
  return m_forwardState.m_updatedState;
}

template<>
inline const TrackSymMatrix& FitNode::get<Op::Forward, Op::Covariance> () const {
  return m_forwardState.m_updatedCovariance;
}

template<>
inline const TrackMatrix<25>& FitNode::get<Op::Forward, Op::TransportMatrix> () const {
  return m_forwardTransportMatrix;
}

template<>
inline const PRECISION& FitNode::get<Op::Backward, Op::Chi2> () const {
  return *m_backwardState.m_chi2;
}

template<>
inline const TrackVector& FitNode::get<Op::Backward, Op::StateVector> () const {
  return m_backwardState.m_updatedState;
}

template<>
inline const TrackSymMatrix& FitNode::get<Op::Backward, Op::Covariance> () const {
  return m_backwardState.m_updatedCovariance;
}

template<>
inline const TrackMatrix<25>& FitNode::get<Op::Backward, Op::TransportMatrix> () const {
  return m_backwardTransportMatrix;
}

template<>
inline const TrackVector& FitNode::get<Op::Smooth, Op::StateVector> () const {
  return m_smoothState.m_state;
}

template<>
inline const TrackSymMatrix& FitNode::get<Op::Smooth, Op::Covariance> () const {
  return m_smoothState.m_covariance;
}

template<>
inline const PRECISION& FitNode::get<Op::Smooth, Op::Residual> () const {
  return *(m_smoothState.m_residual);
}

template<>
inline const PRECISION& FitNode::get<Op::Smooth, Op::ErrResidual> () const {
  return *(m_smoothState.m_errResidual);
}

// -------------------------
// ref getters for the above
// -------------------------

template<>
inline PRECISION& FitNode::get<Op::NodeParameters, Op::ReferenceResidual> () {
  return *(m_nodeParameters.m_referenceResidual);
}

template<>
inline PRECISION& FitNode::get<Op::NodeParameters, Op::ErrMeasure> () {
  return *(m_nodeParameters.m_errorMeasure);
}

template<>
inline TrackVector& FitNode::get<Op::NodeParameters, Op::ReferenceVector> () {
  return m_nodeParameters.m_referenceVector;
}

template<>
inline TrackVector& FitNode::get<Op::NodeParameters, Op::ProjectionMatrix> () {
  return m_nodeParameters.m_projectionMatrix;
}

template<>
inline TrackSymMatrix& FitNode::get<Op::NodeParameters, Op::NoiseMatrix> () {
  return m_nodeParameters.m_noiseMatrix;
}

template<>
inline TrackVector& FitNode::get<Op::NodeParameters, Op::TransportVector> () {
  return m_nodeParameters.m_transportVector;
}

template<>
inline PRECISION& FitNode::get<Op::Forward, Op::Chi2> () {
  return *m_forwardState.m_chi2;
}

template<>
inline TrackVector& FitNode::get<Op::Forward, Op::StateVector> () {
  return m_forwardState.m_updatedState;
}

template<>
inline TrackSymMatrix& FitNode::get<Op::Forward, Op::Covariance> () {
  return m_forwardState.m_updatedCovariance;
}

template<>
inline TrackMatrix<25>& FitNode::get<Op::Forward, Op::TransportMatrix> () {
  return m_forwardTransportMatrix;
}

template<>
inline PRECISION& FitNode::get<Op::Backward, Op::Chi2> () {
  return *m_backwardState.m_chi2;
}

template<>
inline TrackVector& FitNode::get<Op::Backward, Op::StateVector> () {
  return m_backwardState.m_updatedState;
}

template<>
inline TrackSymMatrix& FitNode::get<Op::Backward, Op::Covariance> () {
  return m_backwardState.m_updatedCovariance;
}

template<>
inline TrackMatrix<25>& FitNode::get<Op::Backward, Op::TransportMatrix> () {
  return m_backwardTransportMatrix;
}

template<>
inline TrackVector& FitNode::get<Op::Smooth, Op::StateVector> () {
  return m_smoothState.m_state;
}

template<>
inline TrackSymMatrix& FitNode::get<Op::Smooth, Op::Covariance> () {
  return m_smoothState.m_covariance;
}

template<>
inline PRECISION& FitNode::get<Op::Smooth, Op::Residual> () {
  return *(m_smoothState.m_residual);
}

template<>
inline PRECISION& FitNode::get<Op::Smooth, Op::ErrResidual> () {
  return *(m_smoothState.m_errResidual);
}
