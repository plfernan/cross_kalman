#pragma once

#include "../Types.h"
#include "Predict.h"
#include "Update.h"

namespace VectorFit {

namespace Scalar {

template<class T>
inline void fit (
  FitNode& node,
  const TrackSymMatrixContiguous& covariance
) {
  node.get<T, Op::StateVector>().copy(node.get<Op::NodeParameters, Op::ReferenceVector>());
  node.get<T, Op::Covariance>().copy(covariance);

  update<T>(node);
}

template<class T, bool U>
inline void fit (
  FitNode& node,
  const FitNode& prevnode
) {
  predict<T, U>(node, prevnode);
  update<T>(node);
}

}

}
