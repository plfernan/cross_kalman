#pragma once

#include <vector>
#include <list>
#include "AlignedAllocator.h"
#include "VectorConfiguration.h"
#include "assert.h"

namespace VectorFit {

namespace Mem {

struct Store {
  std::list<std::vector<PRECISION, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>> backends;
  std::list<std::vector<PRECISION, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>>::iterator currentBackend;
  size_t width = 21;
  unsigned currentElement = 0;
  unsigned currentVector = 0;
  unsigned elementCapacity = 0;
  unsigned vectorCapacity = 0;

  Store () = default;

  inline Store (
    const size_t& width,
    const unsigned& numberOfNodes
  ) : width(width) {
    elementCapacity = numberOfNodes;
    backends.emplace_back(elementCapacity * width);
    vectorCapacity = elementCapacity / VECTOR_WIDTH;
    reset();
  }

  void printState (const std::string& position) {
    std::cout << "State at " << position << std::endl
      // << " currentBackend " << currentBackend
      << ", currentVector " << currentVector
      << ", currentElement " << currentElement << std::endl
      << " backend capacity " << backends.size()
      << ", element capacity " << elementCapacity
      << ", vector capacity " << vectorCapacity << std::endl;
  }

  inline void reset () {
    currentBackend = backends.begin();
    currentVector = 0;
    currentElement = 0;
  }

  /**
   * @brief      Aligns to point to a new vector.
   */
  inline void align () {
    if (currentElement != 0) {
      currentElement = 0;
      ++currentVector;

      if (currentVector == vectorCapacity) {
        currentVector = 0;

        if (std::next(currentBackend) == backends.end()) {
          backends.emplace_back(elementCapacity * width);
        }

        ++currentBackend;
      }
    }
  }

  inline PRECISION* getNewVector () {
    align();

    PRECISION* d = getPointer(currentBackend, currentVector);
    
    ++currentVector;
    if (currentVector == vectorCapacity) {
      currentVector = 0;

      if (std::next(currentBackend) == backends.end()) {
        backends.emplace_back(elementCapacity * width);
      }

      ++currentBackend;
    }

    return d;
  }

  inline PRECISION* getNextElement () {
    PRECISION* d = getPointer(currentBackend, currentVector, currentElement);

    ++currentElement;
    if (currentElement == VECTOR_WIDTH) {
      ++currentVector;
      currentElement = 0;

      if (currentVector == vectorCapacity) {
        currentVector = 0;

        if (std::next(currentBackend) == backends.end()) {
          backends.emplace_back(elementCapacity * width);
        }

        ++currentBackend;
      }
    }

    return d;
  }

  inline PRECISION* getLastVector () {
    assert(currentVector!=0 or currentBackend!=backends.begin());

    if (currentVector == 0) {
      return getPointer(std::prev(currentBackend), vectorCapacity - 1);
    }
    return getPointer(currentBackend, currentVector - 1);
  }

  inline PRECISION* getFirstVector () {
    return backends.front().data();
  }

  inline PRECISION* getPointer (
    const std::list<std::vector<PRECISION, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>>::iterator& backend,
    const unsigned& vector,
    const unsigned& element
  ) {
    assert(backend != backends.end() and vector < vectorCapacity and element < VECTOR_WIDTH);

    return backend->data() + vector * VECTOR_WIDTH * width + element;
  }

  inline PRECISION* getPointer (
    const std::list<std::vector<PRECISION, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>>::iterator& backend,
    const unsigned& vector
  ) {
    return getPointer(backend, vector, 0);
  }
};

}

}
