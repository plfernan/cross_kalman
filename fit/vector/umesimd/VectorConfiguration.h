#pragma once

#include "../../../umesimd/UMESimd.h"
using namespace UME::SIMD;

#ifdef SP

#if defined(__AVX512F__) || defined(__AVX512__)
template<> struct Vectype<16> { using type = SIMD16_32f; using booltype = SIMDMask16; };
#endif
#if defined(__AVX__)
template<> struct Vectype<8> { using type = SIMD8_32f; using booltype = SIMDMask8; };
#endif
#if defined(__SSE__) || defined(__ALTIVEC__) || defined(__aarch64__)
template<> struct Vectype<4> { using type = SIMD4_32f; using booltype = SIMDMask4; };
#endif
template<> struct Vectype<1> { using type = SIMD1_32f; using booltype = SIMDMask1; };

#else

#if defined(__AVX512F__) || defined (__AVX512__)
template<> struct Vectype<8> { using type = SIMD8_64f; using booltype = SIMDMask8; };
#endif
#if defined(__AVX__)
template<> struct Vectype<4> { using type = SIMD4_64f; using booltype = SIMDMask4; };
#endif
#if defined(__SSE__) || defined(__ALTIVEC__) || defined(__aarch64__)
template<> struct Vectype<2> { using type = SIMD2_64f; using booltype = SIMDMask2; };
#endif
template<> struct Vectype<1> { using type = SIMD1_64f; using booltype = SIMDMask1; };

#endif

