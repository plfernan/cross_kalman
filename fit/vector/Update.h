#pragma once

#include "../Types.h"
#ifdef USE_VCL
  #include "vcl/Math.h"
#elif defined(USE_UMESIMD)
  #include "umesimd/Math.h"
#endif

namespace VectorFit {

namespace Vector {

template<unsigned W>
inline void update (
  const std::array<Sch::Item, W>& nodes_p,
  const std::vector<VectorFit::Track>& tracks_p,
  fp_ptr_64_const rv_p,
  fp_ptr_64_const pm_p,
  fp_ptr_64_const rr_p,
  fp_ptr_64_const em_p,
  fp_ptr_64 us_p,
  fp_ptr_64 uc_p,
  fp_ptr_64 chi2_p
) {
  MathCommon<W>::update (
    nodes_p,
    tracks_p,
    rv_p,
    pm_p,
    rr_p,
    em_p,
    us_p,
    uc_p,
    chi2_p
  );
}

template<unsigned W>
inline void update (
  const std::array<Sch::Item, W>& nodes_p,
  const std::vector<VectorFit::Track>& tracks_p,
  fp_ptr_64_const ps_p,
  fp_ptr_64_const pc_p,
  fp_ptr_64_const rv_p,
  fp_ptr_64_const pm_p,
  fp_ptr_64_const rr_p,
  fp_ptr_64_const em_p,
  fp_ptr_64 us_p,
  fp_ptr_64 uc_p,
  fp_ptr_64 chi2_p
) {
  MathCommon<W>::update (
    nodes_p,
    tracks_p,
    ps_p,
    pc_p,
    rv_p,
    pm_p,
    rr_p,
    em_p,
    us_p,
    uc_p,
    chi2_p
  );
}

}

}
