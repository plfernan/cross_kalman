#pragma once

#include "MemStore.h"

namespace VectorFit {

namespace Mem {

/**
 * @brief      Effectively a sort of Store iterator
 */
struct Iterator {
  Store* store = nullptr;
  std::list<std::vector<PRECISION, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>>::iterator backend;
  unsigned vector = 0;
  unsigned element = 0;
  unsigned vectorCapacity = 0;

  Iterator () = default;
  
  Iterator (Store& storeParam)
    : store(&storeParam) {
    backend = store->backends.begin();
    vector = 0;
    element = 0;
    vectorCapacity = store->vectorCapacity;
  }

  Iterator (Store& storeParam, const bool& fromCurrent)
    : store(&storeParam) {
    if (fromCurrent) {
      backend = store->currentBackend;
      vector = store->currentVector;
      element = store->currentElement;
      vectorCapacity = store->vectorCapacity;
    } else {
      Iterator(store);
    }
  }

  void printState () {
    std::cout << "Iterator state" << std::endl
      // << " backend " << backend
      << ", vector " << vector << std::endl;
  }

  /**
   * @brief      Gets the next store vector,
   *             and updates the internal state
   *
   * @return     Pointer to next vector
   */
  PRECISION* nextVector () {
    assert(
      (store->currentVector==0) ?
      (backend != std::prev(store->currentBackend) or vector != (vectorCapacity)) :
      (backend != (store->currentBackend) or vector != (store->currentVector))
    );

    PRECISION* d = store->getPointer(backend, vector);

    element = 0;
    ++vector;
    if (vector == vectorCapacity) {
      vector = 0;
      ++backend;
    }

    return d;
  }

  PRECISION* nextElement () {
    assert(backend != (store->currentBackend) or vector != (store->currentVector) or element != (store->currentElement));

    PRECISION* d = store->getPointer(backend, vector, element);

    ++element;
    if (element == VECTOR_WIDTH) {
      element = 0;
      ++vector;
      if (vector == vectorCapacity) {
        vector = 0;
        ++backend;
      }
    }

    return d;
  }
};

/**
 * @brief      Effectively a sort of Store reverse iterator
 */
struct ReverseIterator {
  Store* store = nullptr;
  std::list<std::vector<PRECISION, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>>::iterator backend;
  unsigned vector = 0;
  unsigned vectorCapacity = 0;

  ReverseIterator () = default;

  ReverseIterator (Store& storeParam)
    : store(&storeParam) {
    backend = store->currentBackend;
    vector = store->currentVector;
    vectorCapacity = store->vectorCapacity;
  }

  void printState () {
    std::cout << "ReverseIterator state" << std::endl
      // << " backend " << backend
      << ", vector " << vector << std::endl;
  }

  /**
   * @brief      Gets the previous store vector,
   *             and updates the internal state
   *
   * @return     Pointer to previous vector
   */
  PRECISION* previousVector () {
    assert(backend != store->backends.begin() or vector != 0);

    if (vector == 0) {
      --backend;
      vector = vectorCapacity;
    }

    return store->getPointer(backend, --vector);
  }
};

}
}
