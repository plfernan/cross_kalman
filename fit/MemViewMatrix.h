#pragma once

#include <array>
#include "VectorConfiguration.h"

// Include old matrix types
#include "../gaudi/CholeskyDecomp.h"
#include "../gaudi/MatrixTypes.h"

// Typedefs to avoid clashes
typedef TrackVector TrackVectorAOS;
typedef TrackSymMatrix TrackSymMatrixAOS;
typedef TrackMatrix TrackMatrixAOS;

namespace VectorFit {

struct TrackVectorContiguous {
  /*
  This type is aligned (vs TrackVectorAOS) to be able to transpose using aligned instructions.
  i.e. load() vs loada() instructions will fail if not aligned. So we prefer it.
  */
  _aligned std::array<PRECISION, 5> fArray;

  TrackVectorContiguous () = default;
  TrackVectorContiguous (const TrackVectorContiguous&) = default;
  TrackVectorContiguous (const TrackVectorAOS& v) {
    for (unsigned i = 0; i < 5; ++i) {
      fArray[i] = v[i];
    }
  };
  TrackVectorContiguous (
    const PRECISION& a0,
    const PRECISION& a1,
    const PRECISION& a2,
    const PRECISION& a3,
    const PRECISION& a4
  ) {
    fArray[0] = a0;
    fArray[1] = a1;
    fArray[2] = a2;
    fArray[3] = a3;
    fArray[4] = a4;
  }

  PRECISION& operator() (const int x) {return fArray[x]; }
  PRECISION operator() (const int x) const {return fArray[x]; }
  PRECISION& operator[] (const int x) { return fArray[x]; }
  PRECISION operator[] (const int x) const { return fArray[x]; }

  operator TrackVectorAOS () const {
    return TrackVectorAOS(fArray[0], fArray[1], fArray[2], fArray[3], fArray[4]);
  }
};

struct TrackSymMatrixContiguous {
  _aligned std::array<PRECISION, 15> fArray;

  TrackSymMatrixContiguous () = default;
  TrackSymMatrixContiguous (const TrackSymMatrixContiguous&) = default;
  TrackSymMatrixContiguous (const TrackSymMatrixAOS& m) {
    for (unsigned i = 0; i < 15; ++i) {
      fArray[i] = m[i];
    }
  };

  bool InvertChol() {
    ROOT::Math::CholeskyDecomp<PRECISION, 5> decomp(fArray.data());
    return decomp.Invert(fArray.data());
  }

  PRECISION& operator() (const int row, const int col) {
    return row>col ?
      fArray[row*(row+1)/2 + col] :
      fArray[col*(col+1)/2 + row];
  }

  PRECISION operator() (const int row, const int col) const {
    return row>col ?
      fArray[row*(row+1)/2 + col] :
      fArray[col*(col+1)/2 + row];
  }

  PRECISION& operator[] (const int x) { return fArray[x]; }
  PRECISION operator[] (const int x) const { return fArray[x]; }

  operator TrackSymMatrixAOS () const {
    return TrackSymMatrixAOS(fArray[0], fArray[1], fArray[2], fArray[3], fArray[4],
      fArray[5], fArray[6], fArray[7], fArray[8], fArray[9],
      fArray[10], fArray[11], fArray[12], fArray[13], fArray[14]
    );
  }
};

struct TrackMatrixContiguous {
  _aligned std::array<PRECISION, 25> fArray;

  PRECISION& operator() (const int x, const int y) {return fArray[x*5+y]; }
  PRECISION operator() (const int x, const int y) const {return fArray[x*5+y]; }
  PRECISION& operator[] (const int x) { return fArray[x]; }
  PRECISION operator[] (const int x) const { return fArray[x]; }

  operator TrackMatrixAOS () const {
    return TrackMatrixAOS(fArray);
  }
};

namespace Mem {

namespace View {

template<unsigned long N>
struct TrackMatrix {
  constexpr static size_t size () {
    return N;
  }
  PRECISION* m_basePointer = 0x0;
  TrackMatrix () = default;
  TrackMatrix (PRECISION* basePointer) : m_basePointer(basePointer) {}

  /**
   * @brief      Copies v into its state
   *             Assumes m_basePointer is well defined
   */
  template<class T>
  inline void copy (const T& m) {
    for (unsigned i=0; i<N; ++i) {
      this->operator[](i) = m[i];
    }
  }

  template<class T>
  inline void operator+= (const T& m) {
    for (unsigned i=0; i<N; ++i) {
      this->operator[](i) += m[i];
    }
  }

  inline void setBasePointer (const TrackMatrix<N>& v) { m_basePointer = v.m_basePointer; }
  inline void setBasePointer (PRECISION* m_basePointer) { this->m_basePointer = m_basePointer; }
  inline PRECISION& operator[] (const unsigned& i) { return m_basePointer[i * VECTOR_WIDTH]; }
  inline PRECISION operator[] (const unsigned& i) const { return m_basePointer[i * VECTOR_WIDTH]; }

  operator TrackMatrixAOS () const {
    static_assert(N == 25, "Unsupported vector length for cast operator.");
    TrackMatrixAOS m;
    for (unsigned i=0; i<N; ++i) {
      m[i] = this->operator[](i);
    }
    return m;
  }
};

struct TrackVector : public TrackMatrix<5> {
  TrackVector () = default;
  TrackVector (PRECISION* m_basePointer) : TrackMatrix<5>(m_basePointer) {}

  inline void copy (const TrackVector& v) { TrackMatrix<5>::copy(static_cast<TrackMatrix<5>>(v)); }
  inline void operator+= (const TrackVector& v) { TrackMatrix<5>::operator+=(static_cast<TrackMatrix<5>>(v)); }

  inline void copy (const TrackVectorContiguous& v) { TrackMatrix<5>::template copy<TrackVectorContiguous>(v); }
  inline void operator+= (const TrackVectorContiguous& v) { TrackMatrix<5>::template operator+=<TrackVectorContiguous>(v); }

  inline void copy (const TrackVectorAOS& v) { TrackMatrix<5>::template copy<TrackVectorAOS>(v); }

  inline operator TrackVectorContiguous () const {
    TrackVectorContiguous t;
    for (unsigned i=0; i<5; ++i) {
      t[i] = this->operator[](i);
    }
    return t;
  }

  inline operator TrackVectorAOS () const {
    TrackVectorAOS t;
    for (unsigned i=0; i<5; ++i) {
      t[i] = this->operator[](i);
    }
    return t;
  }

  inline operator StateVector () const {
    StateVector sv;
    sv.m_parameters = this->operator TrackVectorAOS();
    return sv;
  }
};

struct TrackSymMatrix : public TrackMatrix<15> {
  TrackSymMatrix () = default;
  TrackSymMatrix (PRECISION* m_basePointer) : TrackMatrix<15>(m_basePointer) {}

  inline void copy (const TrackSymMatrix& v) { TrackMatrix<15>::copy(static_cast<TrackMatrix<15>>(v)); }
  inline void operator+= (const TrackSymMatrix& v) { TrackMatrix<15>::operator+=(static_cast<TrackMatrix<15>>(v)); }
  
  inline void copy (const TrackSymMatrixContiguous& v) { TrackMatrix<15>::template copy<TrackSymMatrixContiguous>(v); }
  inline void operator+= (const TrackSymMatrixContiguous& v) { TrackMatrix<15>::template operator+=<TrackSymMatrixContiguous>(v); }
  
  inline PRECISION& operator() (const unsigned row, const unsigned col) {
    return row>col ?
      m_basePointer[(row*(row+1)/2 + col) * VECTOR_WIDTH] :
      m_basePointer[(col*(col+1)/2 + row) * VECTOR_WIDTH];
  }

  inline PRECISION operator() (const unsigned row, const unsigned col) const {
    return row>col ?
      m_basePointer[(row*(row+1)/2 + col) * VECTOR_WIDTH] :
      m_basePointer[(col*(col+1)/2 + row) * VECTOR_WIDTH];
  }
  
  inline operator TrackSymMatrixContiguous () const {
    TrackSymMatrixContiguous t;
    for (unsigned i=0; i<15; ++i) {
      t[i] = this->operator[](i);
    }
    return t;
  }

  inline operator TrackSymMatrixAOS () const {
    TrackSymMatrixAOS t;
    for (unsigned i=0; i<15; ++i) {
      t[i] = this->operator[](i);
    }
    return t;
  }
};

// Some operators
inline TrackVectorContiguous operator- (const TrackVectorContiguous& A, const TrackVector& B) {
  return TrackVectorContiguous {
    A[0]-B[0],
    A[1]-B[1],
    A[2]-B[2],
    A[3]-B[3],
    A[4]-B[4]
  };
}

// This is not the "real" multiplication but is the one we need
inline PRECISION operator* (const TrackVector& vA, const TrackVector& vB) {
  return 
    vA[0] * vB[0] +
    vA[1] * vB[1] +
    vA[2] * vB[2] +
    vA[3] * vB[3] +
    vA[4] * vB[4];
}

}

}

}
