#!/bin/bash

function measure_cpu_utilization {
  F=cpu_utilization
  S=10

  if [ $# -ge 1 ]; then F=$1; fi
  if [ $# -ge 2 ]; then S=$2; fi

  mkdir $F -p
  rm -f $F/cpu.sar

  sar -o $F/cpu.sar $S > /dev/null 2>&1
}

function ps {
  ps -eo pcpu,pid,user,args | sort -k 1 -r
}

if [ $# -ge 1 ]
then
  $@
fi

