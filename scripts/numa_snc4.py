#!/usr/bin/python

import sys
sub_numa = int(sys.argv[1])
num_processors = int(sys.argv[2])

numa_opts = [("0-15,64-79,128-143,192-207", 4), \
("16-31,80-95,144-159,208-223", 5),\
("32-47,96-111,160-175,224-239", 6), \
("48-63,112-127,176-191,240-255", 7)]

import re
s, mem = numa_opts[sub_numa][0], numa_opts[sub_numa][1]

# Create list from string
m = re.match("(\d+)\-(\d+)\,(\d+)\-(\d+)\,(\d+)\-(\d+)\,(\d+)\-(\d+)", s)
l = []

for i in range(4):
  for j in range(int(m.group((i*2) + 1)), int(m.group((i*2) + 2)) + 1):
    l.append(j)

sys.stdout.write("numactl -m " + str(mem) + " -C ")
for i in range(0, num_processors):
  sys.stdout.write(str(l[i]))
  if i != num_processors - 1:
    sys.stdout.write(",")
