#!/bin/bash

if [ $# -eq 0 ]
then

  echo Usage: ./numactl_explore.sh \<list of program names\>

else

  WARMUP_EXPERIMENTS=10000
  TOTAL_NUMBER_OF_EXPERIMENTS=500000

  PROCESSOR_COUNT=`cat /proc/cpuinfo | grep processor | tail -n1 | awk '{ print ($3 + 1) }'`
  NUMA_NODES_NUMBER=`lscpu | grep NUMA\ node\(s\) | awk '{ print $3 }'`
  PROCESSORS_PER_DOMAIN=`echo ${PROCESSOR_COUNT} ${NUMA_NODES_NUMBER} | awk '{ print $1 / $2 }'`
  NUMBER_OF_EXPERIMENTS=`echo ${TOTAL_NUMBER_OF_EXPERIMENTS} ${NUMA_NODES_NUMBER} | awk '{ print $1 / $2 }'`

  NUMA_DOMAINS="`lscpu | grep NUMA\ node0 | awk '{ print $4 }'`"
  for i in `seq 1 $((${NUMA_NODES_NUMBER} - 1))`
  do
    NUMA_DOMAINS="${NUMA_DOMAINS}_`lscpu | grep NUMA\ node${i} | awk '{ print $4 }'`"
  done

  echo Detected ${NUMA_NODES_NUMBER} NUMA domains with ${PROCESSORS_PER_DOMAIN} cores each

  echo Running scalability tests for programs $@
  for i in $@
  do
    mkdir -p output/${HOSTNAME}/scalability_${i}
  done

  i=1
  while [ ${i} -le $((${PROCESSORS_PER_DOMAIN} * ${NUMA_NODES_NUMBER})) ]
  do
    echo $i
    for program in $@
    do
      DOMAINS_TO_EXECUTE=$((${NUMA_NODES_NUMBER} - 1))
      PROCESSES_PER_DOMAIN=$((${i} / ${NUMA_NODES_NUMBER}))

      if [ ${i} -lt ${NUMA_NODES_NUMBER} ]
      then
        DOMAINS_TO_EXECUTE=$((${i}-1))
        PROCESSES_PER_DOMAIN=1
      fi

      for j in `seq 0 ${DOMAINS_TO_EXECUTE}`
      do
        ( `./scripts/numa_xeon.py ${NUMA_DOMAINS} ${j} ${PROCESSES_PER_DOMAIN}` ./${program} -w ${WARMUP_EXPERIMENTS} -n ${NUMBER_OF_EXPERIMENTS} > output/${HOSTNAME}/scalability_${program}/${i}_${j}.out ) &
      done
      wait
    done
    
    if [ ${i} -eq 1 ]
    then
      i=2
    elif [ ${i} -eq 2 ]
    then
      i=4
    else
      i=$((${i}+4))
    fi
  done

fi
