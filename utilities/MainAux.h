#pragma once

#include <vector>
#include <array>
#include "../fit/Scheduler.h"
#include "../fit/MemStore.h"
#include "../fit/MemIterator.h"
#include "../fit/MemView.h"

/**
 * @brief      Initializes datatypes for experiments.
 */
void initializeSchedulerAndIterators(
  std::vector<std::vector<Instance>>& events,
  std::vector<std::vector<std::array<std::vector<VectorFit::Sch::Blueprint<VECTOR_WIDTH>>, 5>>>& schedulers,
  std::vector<std::vector<VectorFit::Mem::Iterator>>& nodeParametersIteratorInit,
  std::vector<std::vector<VectorFit::Mem::Iterator>>& transportForwardIteratorInit,
  std::vector<std::vector<VectorFit::Mem::ReverseIterator>>& nodeParametersReverseIteratorInit,
  std::vector<std::vector<VectorFit::Mem::ReverseIterator>>& transportBackwardIteratorInit,
  VectorFit::Mem::Store& nodeParametersStore,
  VectorFit::Mem::Store& transportForwardStore,
  VectorFit::Mem::Store& transportBackwardStore
) {
  for (unsigned i=0; i<events.size(); ++i) {
    auto& event = events[i]; 

    // We reserve mem for the iterators
    nodeParametersIteratorInit[i].resize(event.size());
    transportForwardIteratorInit[i].resize(event.size());
    nodeParametersReverseIteratorInit[i].resize(event.size());
    transportBackwardIteratorInit[i].resize(event.size());
    
    schedulers[i].resize(event.size());

    for (unsigned j=0; j<event.size(); ++j) {
      auto& instance = event[j];
      const auto& typecastedTracks = instance.typecastedTracks;
      auto& tracks = instance.tracks;

      // we create the schedulers pre, main post ([0][1][2]) 
      schedulers[i][j][VectorFit::Sch::Num::Pre] = VectorFit::Sch::StaticScheduler<VectorFit::Sch::Pre, VECTOR_WIDTH>::generate(tracks);
      schedulers[i][j][VectorFit::Sch::Num::Main] = VectorFit::Sch::StaticScheduler<VectorFit::Sch::Main, VECTOR_WIDTH>::generate(tracks);
      schedulers[i][j][VectorFit::Sch::Num::Post] = VectorFit::Sch::StaticScheduler<VectorFit::Sch::Post, VECTOR_WIDTH>::generate(tracks);
      schedulers[i][j][VectorFit::Sch::Num::ForwardInit] = VectorFit::Sch::StaticScheduler<VectorFit::Sch::ForwardInit, VECTOR_WIDTH>::generate(tracks);
      schedulers[i][j][VectorFit::Sch::Num::BackwardInit] = VectorFit::Sch::StaticScheduler<VectorFit::Sch::BackwardInit, VECTOR_WIDTH>::generate(tracks);

      auto setBasePointers = [&] (
        decltype (schedulers[0][0][0])& scheduler, 
        const bool& populateForward,
        const bool& populateBackward
      ) {
        for (const auto& s : scheduler) {
          const auto& in = s.in;
          const auto& out = s.out;
          const auto& action = s.action;
          const auto& pool = s.pool;

          // ensure vectors are aligned (they are already aligned)
          nodeParametersStore.getNewVector();
          if (populateForward)  { transportForwardStore.getNewVector(); }
          if (populateBackward) { transportBackwardStore.getNewVector(); }

          for (unsigned i=0; i<VECTOR_WIDTH; ++i) { 
            if (action [i]) {
              const auto& typecastedNode = typecastedTracks[pool[i].trackIndex][pool[i].nodeIndex];
              auto& track = tracks[pool[i].trackIndex];
              auto& node = track.m_nodes[pool[i].nodeIndex];

              node.m_nodeParameters.setBasePointer(nodeParametersStore.getLastVector() + i);
              node.get<VectorFit::Op::NodeParameters, VectorFit::Op::ReferenceVector>().copy(typecastedNode.m_refVector.m_parameters);
              node.get<VectorFit::Op::NodeParameters, VectorFit::Op::ProjectionMatrix>().copy(typecastedNode.m_projectionMatrix);
              node.get<VectorFit::Op::NodeParameters, VectorFit::Op::NoiseMatrix>().copy(typecastedNode.m_noiseMatrix);
              node.get<VectorFit::Op::NodeParameters, VectorFit::Op::TransportVector>().copy(typecastedNode.m_transportVector);
              node.get<VectorFit::Op::NodeParameters, VectorFit::Op::ReferenceResidual>() = typecastedNode.m_refResidual;
              node.get<VectorFit::Op::NodeParameters, VectorFit::Op::ErrMeasure>() = typecastedNode.m_errMeasure;

              if (populateForward) {
                node.get<VectorFit::Op::Forward, VectorFit::Op::TransportMatrix>().setBasePointer(transportForwardStore.getLastVector() + i);
                node.get<VectorFit::Op::Forward, VectorFit::Op::TransportMatrix>().copy(typecastedNode.m_transportMatrix);
              }

              if (populateBackward) {
                node.get<VectorFit::Op::Backward, VectorFit::Op::TransportMatrix>().setBasePointer(transportBackwardStore.getLastVector() + i);
                node.get<VectorFit::Op::Backward, VectorFit::Op::TransportMatrix>().copy(
                  typecastedTracks[pool[i].trackIndex][pool[i].nodeIndex + 1].m_invertTransportMatrix
                );
              }
            }
          }
        }
      };

      // true / false indicates whether to populate the transport matrix or not
      setBasePointers(schedulers[i][j][VectorFit::Sch::Num::Pre], false, true);

      nodeParametersIteratorInit[i][j] = VectorFit::Mem::Iterator(nodeParametersStore, true); // fromCurrent (to get from we can write)
      transportForwardIteratorInit[i][j] = VectorFit::Mem::Iterator(transportForwardStore, true); 
      
      setBasePointers(schedulers[i][j][VectorFit::Sch::Num::ForwardInit], false, true);
      setBasePointers(schedulers[i][j][VectorFit::Sch::Num::Main], true, true);
      setBasePointers(schedulers[i][j][VectorFit::Sch::Num::BackwardInit], true, false);

      nodeParametersReverseIteratorInit[i][j] = VectorFit::Mem::ReverseIterator(nodeParametersStore);
      transportBackwardIteratorInit[i][j] = VectorFit::Mem::ReverseIterator(transportBackwardStore);

      setBasePointers(schedulers[i][j][VectorFit::Sch::Num::Post], true, false);
    }
  }
}

void clearTypecastedTracks (std::vector<std::vector<Instance>>& events) {
  // We don't need the original tracks datatype anymore
  for (auto& event : events) {
    for (auto& instance : event) {
      instance.typecastedTracks.clear();
    }
  }
}

/**
 * @brief      Converts literal quantities to nums
 */
int convertAmount(std::string amount) {
    std::string str2 = "k";
    std::size_t found = amount.find(str2);
    if (found != std::string::npos) {
        amount.replace(found,str2.length(),"000");
        return std::stoi(amount);
    }

    str2 = "M";
    found = amount.find(str2);
    if (found != std::string::npos) {
        amount.replace(found,str2.length(),"000000");
        return std::stoi(amount);
    }

    return std::stoi(amount);
}

