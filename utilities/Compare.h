#pragma once

#include "../gaudi/Types.h"
#include "../fit/Types.h"
#include <iostream>
#include <string>

long double log2 (const long long x);

bool compare (const TrackVector& m1, const TrackVector& m2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const TrackMatrix& m1, const TrackMatrix& m2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const TrackSymMatrix& m1, const TrackSymMatrix& m2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const ChiSquare& c1, const ChiSquare& c2, const double max_epsilon=1e-03, const bool doPrint=false);
bool compare (const FitNode& f1, const FitNode& f2, const double max_epsilon=1e-03, const bool doPrint=false, const int compareChi2=-1);
