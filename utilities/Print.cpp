#include "Print.h"

void print (uint16_t b) {
  packed16bits* a = (packed16bits*) &b;
  std::cout << a->p << a->o << a->n << a->m;
  #if VECTOR_WIDTH >= 8
  std::cout << a->l << a->k << a->j << a->i;
  #endif
  #if VECTOR_WIDTH == 16
  std::cout << a->h << a->g << a->f << a->e
    << a->d << a->c << a->b << a->a;
  #endif
  std::cout << " ";
}

void print(const TrackVector& m, const std::string& name) {
  std::cout << name << ": ";
  for (unsigned i=0; i<5; ++i) std::cout << m.fArray[i] << ", ";
  std::cout << std::endl;
}

void print(const TrackMatrix& m, const std::string& name) {
  std::cout << name << ": ";
  for (unsigned i=0; i<25; ++i) std::cout << m.fArray[i] << ", ";
  std::cout << std::endl;
}

void print(const TrackSymMatrix& m, const std::string& name) {
  std::cout << name << ": ";
  for (unsigned i=0; i<15; ++i) std::cout << m.fArray[i] << ", ";
  std::cout << std::endl;
}

void print(const ChiSquare& c, const std::string& name) {
  std::cout << name << ":" << std::endl
    << "  m_chi2: " << c.m_chi2 << std::endl
    << "  m_nDoF: " << c.m_nDoF << std::endl;
}

void print(const XYZVector& x, const std::string& name) {
  std::cout << name << ":" << std::endl
    << "  x, y, z: " << x.fX << ", " << x.fY << ", " << x.fZ << std::endl;
}

void print(const State& s, const std::string& name) {
  std::cout << name << ":" << std::endl;
  // std::cout << "  m_flags: " << s.m_flags << std::endl;
  print(s.m_stateVector, "  m_stateVector");
  print(s.m_covariance, "  m_covariance");
  // std::cout << "  m_z: " << s.m_z << std::endl;
}

void print(const StateVector& s, const std::string& name) {
  std::cout << name << ":" << std::endl;
  print(s.m_parameters, "  m_parameters");
  // std::cout << "  m_z: " << s.m_z << std::endl;
}

void print(const Node& n, const std::string& name) {
  std::cout << name << ":" << std::endl;
  print(n.m_state, " smoothed state");
  std::cout << " Type: " << n.m_type << std::endl;
  print(n.m_refVector, " reference vector");
  print(n.m_projectionMatrix, " Projection Matrix");
  // std::cout << " m_refIsSet: " << n.m_refIsSet << std::endl
    // << " m_measurement: " << n.m_measurement << std::endl
  std::cout 
    // << " m_residual: " << n.m_residual << std::endl
    // << " m_errResidual: " << n.m_errResidual << std::endl
    << " Error measure: " << n.m_errMeasure << std::endl;
  
}

void print(const FitNode& f, const std::string& name) {
  std::cout << name << ": " << std::endl;
  // print(f.m_noiseMatrix, " m_noiseMatrix");
  // std::cout << " m_deltaEnergy: " << f.m_deltaEnergy << std::endl
  //   << " m_filterStatus: " << f.m_filterStatus[0] << ", " << f.m_filterStatus[1] << std::endl
  //   << " m_hasInfoUpstream: " << f.m_hasInfoUpstream[0] << ", " << f.m_hasInfoUpstream[1] << std::endl;
  std::cout << " Forward:" << std::endl;
  // print(f.m_predictedState[0], " predicted state");
  print(f.m_filteredState[0], " updated state");
  print(f.m_totalChi2[0], " chi2");
  std::cout << " Backward:" << std::endl;
  // print(f.m_predictedState[1], " predicted state");
  print(f.m_filteredState[1], " updated state");
  print(f.m_totalChi2[1], " chi2");
  print(*(dynamic_cast<Node*>(& const_cast<FitNode&>(f))), " Node");
  std::cout << " Reference Residual: " << f.m_refResidual << std::endl;
  // print(f.m_transportMatrix, " Transport Matrix");
  // print(f.m_invertTransportMatrix, " m_invertTransportMatrix");
  // print(f.m_transportVector, " m_transportVector");
  // print(f.m_classicalSmoothedState, " m_classicalSmoothedState");
  // print(f.m_deltaChi2[0], " m_deltaChi2[0]");
  // print(f.m_deltaChi2[1], " m_deltaChi2[1]");
  // print(f.m_smootherGainMatrix, " m_smootherGainMatrix");
  // print(f.m_pocaVector, " m_pocaVector");
  // std::cout << " m_doca: " << f.m_doca << std::endl
  //   << " m_chi2Double: " << f.m_chi2Double << std::endl;
}

std::string shellexec (const char* cmd) {
  char buffer[128];
  std::string result = "";
  std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
  if (!pipe) throw std::runtime_error("popen() failed!");
  while (!feof(pipe.get())) {
    if (fgets(buffer, 128, pipe.get()) != NULL)
      result += buffer;
  }
  return result;
}
