#pragma once

#include "../gaudi/Types.h"
#include "../fit/Types.h"
#include <cstdio>
#include <memory>

// Print mask
#pragma pack (push)
struct packed16bits {
  bool p : 1;
  bool o : 1;
  bool n : 1;
  bool m : 1;
  bool l : 1;
  bool k : 1;
  bool j : 1;
  bool i : 1;
  bool h : 1;
  bool g : 1;
  bool f : 1;
  bool e : 1;
  bool d : 1;
  bool c : 1;
  bool b : 1;
  bool a : 1;
};
#pragma pack (pop)

void print (uint16_t b);
void print (const TrackVector& m, const std::string& name="TrackVector");
void print (const TrackMatrix& m, const std::string& name="TrackMatrix");
void print (const TrackSymMatrix& m, const std::string& name="TrackSymMatrix");
void print (const XYZVector& x, const std::string& name="XYZVector");
void print (const ChiSquare& c, const std::string& name="ChiSquare");
void print (const State& s, const std::string& name="State");
void print (const StateVector& s, const std::string& name="StateVector");
void print (const Node& n, const std::string& name="Node");
void print (const FitNode& f, const std::string& name="FitNode");

std::string shellexec (const char* cmd);
